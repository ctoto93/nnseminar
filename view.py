import roboschool
import gym
import tensorflow as tf
import numpy as np
from parser import args
from model_path_manager import ModelPathManager

manager = ModelPathManager(args.models_path, args.name)
env = gym.make('RoboschoolWalker2d-v1')
A_BOUND = [env.action_space.low, env.action_space.high]

saver = tf.train.import_meta_graph(manager.model_meta())
graph = tf.get_default_graph()
init = tf.global_variables_initializer()
S = graph.get_tensor_by_name("Global_Net/S:0")
# a_params = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope='Global_Net/actor')
mu = graph.get_tensor_by_name('Global_Net/actor/mu/bias:0')
sigma = graph.get_tensor_by_name('Global_Net/actor/sigma/bias:0')
mu, sigma = mu * A_BOUND[1], sigma + 1e-4
normal_dist = tf.contrib.distributions.Normal(mu, sigma)
actor = tf.clip_by_value(tf.squeeze(normal_dist.sample(1), axis=0), A_BOUND[0], A_BOUND[1])

with tf.Session() as sess:
    sess.run(init)
    while True:
        s = env.reset()
        done = False
        total_reward = 0
        while not done:
            env.render()
            s = s[np.newaxis, :]
            action = sess.run([actor], feed_dict={S: s})[0]
            s, reward, done, info = env.step(action)
            total_reward += reward

        print('Total Reward: {}'.format(total_reward))
