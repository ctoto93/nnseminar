import json
from collections import namedtuple

class Config:
    DEFAULT_MAX_EP_STEP = 10           # maxumum number of steps per episode
    DEFAULT_MAX_GLOBAL_EP = 10000        # total number of episodes
    DEFAULT_GAMMA = 0.90                # discount factor
    DEFAULT_ENTROPY_BETA = 0.02         # entropy factor
    DEFAULT_LR_A = 0.001               # learning rate for actor
    DEFAULT_LR_C = 0.001                # learning rate for critic
    DEFAULT_HN_1 = 50
    DEFAULT_HN_2 = 25

    def __init__(self, **kwargs):
        prop_defaults = {
            "max_global_ep": Config.DEFAULT_MAX_GLOBAL_EP,
            "max_ep_step": Config.DEFAULT_MAX_EP_STEP,
            "gamma": Config.DEFAULT_GAMMA,
            "entropy_beta": Config.DEFAULT_ENTROPY_BETA,
            "lr_a": Config.DEFAULT_LR_A,
            "lr_c": Config.DEFAULT_LR_C,
            "hn_1": Config.DEFAULT_HN_1,
            "hn_2": Config.DEFAULT_HN_2,
            "random_seed": 42
        }

        for (prop, default) in prop_defaults.items():
            setattr(self, prop, kwargs.get(prop, default))
