class ModelPathManager:
    def __init__(self, path, name):
        self.path = path
        self.name = name

    def _path(self):
        return "{}/{}/".format(self.path, self.name)
    def global_data(self):
        return "{}/global.data".format(self._path())
    def plot(self):
        return "{}/plot.png".format(self._path())
    def model(self):
        return "{}/model.ckpt".format(self._path())
    def model_meta(self):
        return "{}/model.ckpt.meta".format(self._path())
    def log(self):
        return "{}/log".format(self._path())
    def config(self):
        return "{}/config.json".format(self._path())
