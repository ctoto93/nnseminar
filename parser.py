import argparse

DEFAULT_MODEL_NAME='my_model';
DEFAULT_MODELS_PATH='./models';

parser = argparse.ArgumentParser()
parser.add_argument('--name', nargs='?', default=DEFAULT_MODEL_NAME, type=str)
parser.add_argument('--models_path', nargs='?', default=DEFAULT_MODELS_PATH, type=str)
parser.add_argument("--resume", default=False, action="store_true" , help="resume train data")
args = parser.parse_args()
