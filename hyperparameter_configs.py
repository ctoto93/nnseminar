import json
import subprocess
import os
from parser import args
import pathlib

RANDOM_SEEDS = [42, 3, 100]
N_SAMPLE_MODEL = len(RANDOM_SEEDS)


if __name__ == "__main__":
    with open("hyperparameter_configs.json") as f:
        configs = json.load(f)

    for c in configs:
        model_name = c["name"]
        for i in range(N_SAMPLE_MODEL):
            c["name"] = "{}_{}".format(model_name, i)
            c["random_seed"] = RANDOM_SEEDS[i]
            path = "{}/{}".format(args.models_path, c["name"])
            pathlib.Path(path).mkdir(parents=True, exist_ok=True)
            config_path = "{}/config.json".format(path)

            with open(config_path, "w+") as wf:
                json.dump(c, wf, indent=4)
            subprocess.call(['pipenv', 'run', 'python', 'a3c.py', '--name={}'.format(c["name"])])
