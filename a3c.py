import roboschool
import gym
import multiprocessing
import threading
import numpy as np
import os
import shutil
import tensorflow as tf
import matplotlib
import json
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import pickle
from parser import args
from model_path_manager import ModelPathManager
from config import Config

#load Config
MODEL_PATH_MANAGER = ModelPathManager(args.models_path, args.name)
with open(MODEL_PATH_MANAGER.config()) as f:
    conf = json.load(f)
    CONFIG = Config(**conf)

#PARAMETERS
OUTPUT_GRAPH = True         # safe logs
RENDER=False
N_WORKERS = multiprocessing.cpu_count() # number of workers
MAX_EP_STEP = CONFIG.max_ep_step           # maxumum number of steps per episode
MAX_GLOBAL_EP = CONFIG.max_global_ep        # total number of episodes
MAX_GLOBAL_REWARD = 200
GLOBAL_NET_SCOPE = 'Global_Net'
GAMMA = CONFIG.gamma                # discount factor
ENTROPY_BETA = CONFIG.entropy_beta         # entropy factor
LR_A = CONFIG.lr_a               # learning rate for actor
LR_C = CONFIG.lr_c                # learning rate for critic
HN_1 = CONFIG.hn_1
HN_2 = CONFIG.hn_2
RANDOM_SEED = CONFIG.random_seed

# initialize random seed
tf.random.set_random_seed(RANDOM_SEED)

# set environment
GAME = 'RoboschoolWalker2d-v1'
env = gym.make(GAME)
env.reset()
# if RENDER:                 # uncomment if rendering does not work
#    env.render()
N_S = env.observation_space.shape[0]                    # number of states
N_A = env.action_space.shape[0]                         # number of actions
A_BOUND = [env.action_space.low, env.action_space.high] # action bounds

# Network for the Actor Critic
class ACNet(object):
    def __init__(self, scope, sess, globalAC=None):
        self.sess=sess
        self.actor_optimizer = tf.train.RMSPropOptimizer(LR_A, name='RMSPropA')  # optimizer for the actor
        self.critic_optimizer = tf.train.RMSPropOptimizer(LR_C, name='RMSPropC') # optimizer for the critic

        if scope == GLOBAL_NET_SCOPE:   # get global network
            with tf.variable_scope(scope):
                self.s = tf.placeholder(tf.float32, [None, N_S], 'S')       # state
                self.a_params, self.c_params = self._build_net(scope)[-2:]  # parameters of actor and critic net
        else:   # local net, calculate losses
            with tf.variable_scope(scope):
                self.s = tf.placeholder(tf.float32, [None, N_S], 'S')            # state
                self.a_his = tf.placeholder(tf.float32, [None, N_A], 'A')        # action
                self.v_target = tf.placeholder(tf.float32, [None, 1], 'Vtarget') # v_target value

                mu, sigma, self.v, self.a_params, self.c_params = self._build_net(scope) # get mu and sigma of estimated action from neural net

                td = tf.subtract(self.v_target, self.v, name='TD_error')
                with tf.name_scope('c_loss'):
                    self.c_loss = tf.reduce_mean(tf.square(td))

                with tf.name_scope('wrap_a_out'):
                    mu, sigma = mu * A_BOUND[1], sigma + 1e-4

                normal_dist = tf.contrib.distributions.Normal(mu, sigma)

                with tf.name_scope('a_loss'):
                    log_prob = normal_dist.log_prob(self.a_his)
                    exp_v = log_prob * td
                    entropy = normal_dist.entropy()  # encourage exploration
                    self.exp_v = ENTROPY_BETA * entropy + exp_v
                    self.a_loss = tf.reduce_mean(-self.exp_v)

                with tf.name_scope('choose_a'):  # use local params to choose action
                    self.A = tf.clip_by_value(tf.squeeze(normal_dist.sample(1), axis=0), A_BOUND[0], A_BOUND[1]) # sample a action from distribution
                with tf.name_scope('local_grad'):
                    self.a_grads = tf.gradients(self.a_loss, self.a_params) #calculate gradients for the network weights
                    self.c_grads = tf.gradients(self.c_loss, self.c_params)

            with tf.name_scope('sync'): # update local and global network weights
                with tf.name_scope('pull'):
                    self.pull_a_params_op = [l_p.assign(g_p) for l_p, g_p in zip(self.a_params, globalAC.a_params)]
                    self.pull_c_params_op = [l_p.assign(g_p) for l_p, g_p in zip(self.c_params, globalAC.c_params)]
                with tf.name_scope('push'):
                    self.update_a_op = self.actor_optimizer.apply_gradients(zip(self.a_grads, globalAC.a_params))
                    self.update_c_op = self.critic_optimizer.apply_gradients(zip(self.c_grads, globalAC.c_params))

    def _build_net(self, scope): # neural network structure of the actor and critic
        w_init = tf.random_normal_initializer(0., .1)
        with tf.variable_scope('actor'):
            l_a1 = tf.layers.dense(self.s, HN_1, tf.nn.relu6, kernel_initializer=w_init, name='la1')
            l_a2 = tf.layers.dense(l_a1, HN_2, tf.nn.relu6, kernel_initializer=w_init, name='la2')
            mu = tf.layers.dense(l_a2, N_A, tf.nn.tanh, kernel_initializer=w_init, name='mu') # estimated action value
            sigma = tf.layers.dense(l_a2, N_A, tf.nn.softplus, kernel_initializer=w_init, name='sigma') # estimated variance
        with tf.variable_scope('critic'):
            l_c1 = tf.layers.dense(self.s, HN_1, tf.nn.relu6, kernel_initializer=w_init, name='lc1')
            l_c2 = tf.layers.dense(l_c1, HN_2, tf.nn.relu6, kernel_initializer=w_init, name='lc2')
            v = tf.layers.dense(l_c2, 1, kernel_initializer=w_init, name='v')  # estimated value for state
        a_params = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=scope + '/actor')
        c_params = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=scope + '/critic')
        return mu, sigma, v, a_params, c_params

    def update_global(self, feed_dict):  # run by a local
        self.sess.run([self.update_a_op, self.update_c_op], feed_dict)  # local grads applies to global net

    def pull_global(self):  # run by a local
        self.sess.run([self.pull_a_params_op, self.pull_c_params_op])

    def choose_action(self, s):  # run by a local
        s = s[np.newaxis, :]
        return self.sess.run(self.A, {self.s: s})[0]

# worker class that inits own environment, trains on it and updloads weights to global net
class Worker(object):
    def __init__(self, name, globalAC, sess, coord):
        self.env = gym.make(GAME).unwrapped   # make environment for each worker
        self.name = name
        self.AC = ACNet(name, sess, globalAC) # create ACNet for each worker
        self.sess=sess
        self.coord = coord

    def is_reward_exceeded(self):
        global global_rewards
        if len(global_rewards) < 5:
            return False
        return global_rewards[-1] > MAX_GLOBAL_REWARD

    def work(self):
        global global_rewards, global_episodes

        while not coord.should_stop() and global_episodes < MAX_GLOBAL_EP:
            s = self.env.reset()
            buffer_s, buffer_a, buffer_r = [], [], []
            ep_r = 0
            for ep_t in range(MAX_EP_STEP):
                a = self.AC.choose_action(s)         # estimate stochastic action based on policy
                s_, r, done, info = self.env.step(a) # make step in environment

                ep_r += r
                # save actions, states and rewards in buffer
                buffer_s.append(s)
                buffer_a.append(a)
                buffer_r.append(r)

                s = s_

                if done:
                    v_s_ = 0   # terminal
                else:
                    v_s_ = self.sess.run(self.AC.v, {self.AC.s: s_[np.newaxis, :]})[0, 0]

                if (ep_t == MAX_EP_STEP - 1 or done):
                    break

            buffer_v_target = []
            for r in buffer_r[::-1]:    # reverse buffer r
                v_s_ = r + GAMMA * v_s_
                buffer_v_target.append(v_s_)
            buffer_v_target.reverse()

            buffer_s, buffer_a, buffer_v_target = np.vstack(buffer_s), np.vstack(buffer_a), np.vstack(buffer_v_target)
            feed_dict = {
                self.AC.s: buffer_s,
                self.AC.a_his: buffer_a,
                self.AC.v_target: buffer_v_target,
            }
            self.AC.update_global(feed_dict) # actual training step, update global ACNet
            self.AC.pull_global() # get global parameters to local ACNet

            global_rewards.append(ep_r)
            print(
                self.name,
                "Ep:", global_episodes,
                "| Ep_r: %i" % ep_r,
                  )
            # if ep_r > 80:
            #     self.coord.request_stop()
            global_episodes += 1



def save_and_plot_model(sess):
    global_data = {
        "global_episodes": global_episodes,
        "global_rewards": global_rewards
    }
    with open(MODEL_PATH_MANAGER.global_data(), 'wb') as fp:
        pickle.dump(global_data, fp)
    saver = tf.train.Saver()
    save_path = saver.save(sess, MODEL_PATH_MANAGER.model())

    plt.plot(np.arange(len(global_rewards)), global_rewards) # plot rewards
    plt.xlabel('episodes')
    plt.ylabel('total moving reward')
    plt.savefig(MODEL_PATH_MANAGER.plot())

if __name__ == "__main__":

    coord = tf.train.Coordinator()
    global_rewards = []
    global_episodes = 0
    sess = tf.Session()

    if args.resume:
        with open(MODEL_PATH_MANAGER.global_data(), 'rb') as fp:
            global_data = pickle.load(fp)
            global_rewards = global_data["global_rewards"]
            global_episodes = global_data["global_episodes"]
            # print('resuming: \nepisodes: %s\nrewards: %s\n' % (global_episodes, global_rewards))
        saver = tf.train.import_meta_graph(MODEL_PATH_MANAGER.model_meta())
        init = tf.global_variables_initializer()
        sess.run(init)
    try:
        with tf.device("/cpu:0"):
            global_ac = ACNet(GLOBAL_NET_SCOPE,sess)  # we only need its params
            workers = []
            # Create workers
            for i in range(N_WORKERS):
                i_name = 'W_%i' % i   # worker name
                workers.append(Worker(i_name, global_ac,sess, coord))


        sess.run(tf.global_variables_initializer())

        if OUTPUT_GRAPH: # write log file
            log_dir = MODEL_PATH_MANAGER.log()
            if os.path.exists(log_dir):
                shutil.rmtree(log_dir)
            tf.summary.FileWriter(log_dir, sess.graph)

        worker_threads = []
        for worker in workers: #start workers
            job = lambda: worker.work()
            t = threading.Thread(target=job)
            t.start()
            worker_threads.append(t)
        coord.join(worker_threads)  # wait for termination of workers
        save_and_plot_model(sess)
    except KeyboardInterrupt:
        coord.request_stop()
        save_and_plot_model(sess)
